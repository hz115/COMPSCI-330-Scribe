\documentclass[11pt]{article}

\usepackage{fullpage}
\usepackage{latexsym}
\usepackage{graphicx,color}
\usepackage{amsmath, amssymb, amsthm, amsfonts}
\definecolor{newblue}{rgb}{0.2,0.2,0.6}
\usepackage[colorlinks,pagebackref,allcolors=newblue]{hyperref}
\usepackage{enumitem}

\newcommand{\handout}[5]{
   \renewcommand{\thepage}{#1-\arabic{page}}
   \noindent
   \begin{center}
   \framebox{
      \vbox{
    \hbox to 5.78in { {\bf COMPSCI 330: Design and Analysis of Algorithms} \hfill #2 }
       \vspace{4mm}
       \hbox to 5.78in { {\Large \hfill #5  \hfill} }
       \vspace{2mm}
       \hbox to 5.78in { {\it #3 \hfill #4} }
      }
   }
   \end{center}
   \vspace*{4mm}
}

\renewcommand{\paragraph}[1]{\medskip \noindent {\bf #1.}}

\newcommand{\lecture}[4]{\handout{#1}{#2}{Lecturer: #3}{Scribe: #4}{#1}}

\theoremstyle{definition}

\newtheorem{definition}{Definition}
\newtheorem{remark}{Remark}
\newtheorem{theorem}{Theorem}
\newtheorem{example}{Example}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{fact}[theorem]{Fact}
%\newcommand{\example}[1]{\paragraph{Example #1}}

\begin{document}

\lecture{Lecture 6: Greedy Algorithms I}{September 14}{Rong Ge}{Fred Zhang}

\section{Overview}
\label{section:overview}
In this lecture, we introduce a new algorithm design technique---\textit{greedy algorithms}. On a high level, it bears the same philosophy as dynamic programming and divide-and-conquer, of breaking a large problem into smaller ones that are simple to solve. 

Although easy to devise, greedy algorithms can be hard to analyze. The  correctness is often established via  proof by contradiction. We demonstrate greedy algorithms for solving \textit{fractional knapsack} and \textit{interval scheduling} problem and analyze their correctness.

\section{Introduction to Greedy Algorithms}
\label{section:intro}
Today we discuss greedy algorithms. This is the third algorithm design technique we have covered in the class and is the last one. Previously, we saw dynamic programming and divide-and-conquer. All these paradigms attempt to break a large, complicated problems into many smaller ones. In divide-and-conquer, the idea is simply to partition the problem into independent parts and solve them. In dynamic programming, we collect a lot of small problems that look similar to the original problem and make a table to solve the small ones. 

Now greedy is probably the most intuitive approach in algorithm design.  If a problem requires to make a sequence of decisions, a greedy algorithm always makes the ''best`` choice given the current situation and never considers the future. You will see what we mean by ``best'' choice and current situation later. This automatically reduces the problem to a smaller subproblem which requires making fewer decisions.  Suppose originally I have to make $n$ decisions, but now I only worry about one local decision. 

For a warm-up, consider you walk in Manhattan, and the destination in northeast. Then walking towards north or east always reduces your distance to the destination. Initially, the choice of going towards which direction does not matter, and you can be greedy.  But consider you are driving, and sometimes the path is one-way. Then greedy does not always work, and you want to plan ahead to make sure you are not blocked by one-way street in future.

\subsection{Design and Analysis}\label{section:design-analysis}
In designing greedy algorithm, we have the following general guideline:
\begin{enumerate}[label=(\roman*),itemsep=0.3pt]
    \item  Break the problem into a sequence of decisions, just like in dynamic programming. But bear in mind that greedy algorithm does not always yield the optimal solution. For example, it is not optimal to run greedy algorithm for Longest Subsequence.
    \item   Identify a rule for the ``best'' option. Once the last step is completed, you immediately want to make the first decision in a greedy manner, without considering other future decisions.
\end{enumerate}
The hard part usually lies in analysis. In divide-and-conquer and dynamic programming, sometimes the proof simply follows from an easy induction, but that is not generally the case in greedy algorithms. The key thing to remember is that greedy algorithm often fails if you cannot find a proof. 

A common proof technique used in proving correctness of greedy algorithms is proof by contradiction. One starts by assuming that there is a better solution, and the goal is to refute it by showing that it is actually not better than what the algorithm did.

\section{Fractional Knapsack}\label{section:knapsack}
Now let us consider our first problem, the \textit{fractional knapsack} problem. It is similar to the knapsack problem we solved via dynamic programming. Recall that we have knapsack of bounded capacity $W$, so it can hold items of total weight at most $W$. There are $n$ items, each of weight $w_1, w_2, \ldots , w_n$. Each item also has a value $v_1, v_2,\ldots,v_n$. The difference of this fractional knapsack is that the items are infinitely divisible. You are allowed to put, say, $1/2$ (or any fraction) of an item into the knapsack. The goal is to select a set of fractions $p_1,p_2,\ldots, p_n$ for all items to maximize the total value
\begin{equation}\label{eqn:value}
p_1v_1 + p_2v_2 +\ldots p_nv_n
\end{equation}
subject to  the capacity constraint
\begin{equation}\label{eqn:cap}
p_1w_1 + p_2w_2 +\ldots p_nw_n \leq W.
\end{equation}
In other words, we allow $p_i$ only to be $0$ or $1$ in the previous version of the knapsack problem, but now they can be anything between $0$ and $1$. 
\begin{example}\label{ex:1}
    Consider an example where the capacity $W=10$ and there are three items 
\begin{table}[htbp]
\centering
\begin{tabular}{l|l|l}
\hline
Item & Weight & Value \\ \hline
\#1    & 6      & 20    \\ \hline
\#2    & 5      & 15    \\ \hline
\#3    & 4      & 10    \\ \hline
\end{tabular}
\end{table}

If this is the integral knapsack problem, we would choose item \#1 and \#3, and this gives me total value $30$. Of course, in fractional knapsack I can choose fractions of items.  It turns out that the optimal solution is choose item \#1 entirely and $0.8$ fraction of  item \#2. The total weight is still $10$, yet the value is now $32$. One can see that allowing fractional solution may give us more valuable solutions. 
\end{example}

\subsection{Algorithm}\label{section:algo}
Let us follow the guideline in Section~\ref{section:design-analysis} to design an algorithm.

\begin{enumerate}[label=(\roman*),itemsep=0.3pt]
    \item We first ask how one should break down the problem, \textit{i.e.}, what decisions one needs to make. Of course, we would like to answer what item I should put into the knapsack and of what fraction. Answering this sequentially eventually yields a solution.
    \item We specify a rule for finding the ``best'' item. We are faced with our first choice---what is the first item I should put into the knapsack?  In this case, the rule is natural: we put in the item with maximum value per unit weight; that is,
        \begin{center}\label{eqn:rule}
            Find an item $i$ such that $v_i/w_i$ is maximized.
        \end{center}
        Now there are two cases. If the knapsack can take in this item entirely, then I am happy to do that. Otherwise, I would put as large fraction as possible.
\end{enumerate}
\begin{example}
    We can check that the \hyperref[eqn:rule]{greedy rule} indeed gives optimal solution for \hyperref[ex:1]{Example 1}.  We first put in item \#1 entirely and second consider item \#2, but since it is too large for the remaining capacity, we put as much as possible, which is a $0.8$ fraction.
\end{example}
How can we implement the algorithm? We first sort all items by their ``value per unit weight'' and put them in a list. Then scan the list in descending order, first consider the highest ``value per  unit weight'', try to put it in, and so on.

\subsection{Analysis}
\paragraph{Running time} The running time of the algorithm is easy to analyze. We sort the items first, which takes $O(n\log n)$ time and the rest takes $O(n)$ time. Therefore, this is an $O(n\log n)$ time algorithm.

\paragraph{Correctness} As said earlier, it can be hard to prove correctness for greedy algorithms.   Here, we present a proof by contradiction.
\begin{theorem}\label{thm:knap}
    The algorithm described in \hyperref[section:algo]{Section 3.1} provides an optimal solution for the fractional knapsack problem.
\end{theorem}
Let me first give a sketch for the proof idea. 
\begin{enumerate}[label=(\roman*),itemsep=0.3pt]
    \item Assume that there is a better solution, in the hope that we see a contradiction.
    \item Argue that the claimed ``better'' solution is in fact not better than the one provided by our algorithm.  (If someone comes up to you claiming that she has a better solution but  you can always refute her, then that means your solution is the best possible.)
\end{enumerate}
\begin{proof}
    The index of the items is irrelevant. Hence, without loss of generality, assume items are sorted in decreasing order of $v_i / w_i$; that is,
\begin{equation}
    v_1/w_1 \geq v_2/v_2 \geq \ldots \geq v_n/w_n
\end{equation}
Suppose the algorithm gives a solution $\textsf{ALG}=(p_1,p_2,\ldots,p_n)$. Only the last non-zero item in the solution is fractional, since the algorithm always tries to put in the item entirely and the last item considered fills the knapsack.

Now assume for a contradiction that there is a better solution. We call this hypothetical solution \textsf{OPT} and $\textsf{OPT}= (q_1,q_2,\ldots,q_n)$. Since \textsf{OPT} is better than \textsf{ALG}, there must be a location where it differs from \textsf{ALG}. Suppose that the first location  of the difference (from left to right) occurs at  item $i$, where $p_i\neq q_1$ but $p_{i'}=q_{i'}$ for all $i' < i$.

Before item $i$, the two solutions are the same, and so are the remaining capacity till this point. By the definition of our algorithm, it tries to put as much item $i$ as possible into the knapsack. Therefore,  \textsf{OPT} does not select more fraction of item $i$ than \textsf{ALG}, \textit{i.e.}, $q_i < p_i$. (Otherwise, \textsf{OPT} would exceed the capacity.)

However, since \textsf{OPT} is better, there must be an item $j$ ($j>i$) such that $p_j < q_j$. Consider one removes $\epsilon$ fraction of item $j$  from \textsf{OPT} and uses the capacity that is freed up to accommodate more fraction of item $i$. Formally,
\begin{align}
    q_j &\leftarrow q_j - \epsilon \nonumber\\
    q_i &\leftarrow q_i +  \frac{\epsilon \cdot w_j}{w_i}\label{eqn:opt2}.
\end{align}
The \hyperref[eqn:opt2]{operation (4)} does not violate capacity constraint because removing $\epsilon$ fraction of item $j$ frees up $\epsilon\cdot w_j$ capacity, which can be used to take $\frac{\epsilon \cdot w_j}{w_i}$ additional fraction of item $i$. We call the modified solution \textsf{OPT}'

We claim that the new solution $\textsf{OPT}'$ is as good as \textsf{OPT}.  The value of this solution is given by
\begin{align*}
    \text{value}(\textsf{OPT}') &= \text{value}(\textsf{OPT}) - \underbrace{\epsilon\cdot v_j}_{\text{loss on item } j} +  \underbrace{\frac{\epsilon \cdot w_j}{w_i} \cdot v_i}_{\text{gain on item } i} \\ 
                                &\geq \text{value}(\textsf{OPT}). 
\end{align*}
The last inequality follows from the fact that $v_i/w_i \geq v_j/w_j$. The gain on item $i$ is at least the loss on item $j$.

After the shift, $\textsf{OPT}'$ is closer to \textsf{ALG}. We repeat this argument until the operation cannot be done. Eventually, \textsf{OPT} becomes \textsf{ALG}, and each step can only increase the value. This implies that value$(\textsf{OPT}) \leq \text{value}(\textsf{ALG})$, a contradiction.
\end{proof}
Admittedly, this proof is more complicated than it needs to be. After class, I will post \href{http://www.cs.duke.edu/courses/fall17/compsci330/lecture6.pdf}{a slightly simpler proof on course website}. The reason we show this complicated proof is that it is easier to generalize to the analysis of  other greedy algorithms.

\section{Interval Scheduling}\label{section:int-sch}
In the remaining time, let us look at another problem, called \textit{interval scheduling}. There are $n$ meeting request, and meeting $i$ takes time $(s_i,t_i)$---it starts at $s_i$ and ends at $t_i$. The constraint is that no two meeting can be scheduled together if their intervals overlap.  Our goal is to schedule as many meetings as possible.
\begin{example}\label{ex:int}
    Suppose we have meetings $(1,3), (2, 4), (4, 5), (4, 6), (6, 8)$ that look like this:
    \begin{figure}[htbp]
        \centering
        \includegraphics[scale=.4]{./img/meetings.png}
        \caption{Meetings' intervals.}
        \label{fig:meet}
    \end{figure}
\end{example}
We should think of intervals as open; that is, $(2,4)$ does not overlap with $(4,5)$. The solution to this instance to schedule three meetings, \textit{e.g.}, $\{(1,3),(4,5),(6,8)\}$. Of course, there can be more than one solutions.

\subsection{Algorithm}\label{section:algo-int}
Let us again follow our \hyperref[section:design-analysis]{guideline for designing greedy algorithms}. 
\begin{enumerate}[label=(\roman*),itemsep=0.3pt]
    \item We ask first what decisions one needs to make. The answer here is kind of trivial. Clearly, we want to schedule intervals.
    \item What is the ``best'' local  option?  In particular, what is the first meeting to schedule?
\end{enumerate}
Intuitively, I would like to earlier meetings to start with, and the earlier the better. But it is not clear what we mean by ``early''. Consider two meetings $(1,4)$ and $(2,3)$. The first starts early and ends late. It is natural to conclude that one should schedule $(2,3)$ because it ends early and allows me to schedule other meetings starting at $3$ onwards. If I scheduled $(1,4)$, I could only start another meeting at $4$. Ending time is what affects future. (Here, we emphasize that the length of the interval is irrelevant. We simply want to maximize total number of scheduled meetings.)

Once you make the first decision, you can use the greedy rule repeatedly to find a solution. Hence, we have the algorithm:
\begin{center}
    Algorithm: always try to schedule the meeting with the earliest \textit{ending} time.
\end{center}
It is simple to implement the algorithm. One starts by sorting all intervals by their ending times in ascending order. Then scan the intervals from the one with the earliest ending time,  try to schedule the current interval, and if there is a conflict, then skip this interval.

\subsection{Analysis}
\paragraph{Running time} It is again easy to see that the algorithm runs in $O(n\log n)$ time, since the most expensive step is sorting.

\paragraph{Correctness} We shall focus on analyzing the correctness of this algorithm  by first understanding the algorithm more concretely.
\begin{example}
Let us consider \hyperref[ex:int]{Example 3} again and see what our algorithm will do. 
\begin{enumerate}[label=(\arabic*),itemsep=0.3pt]
    \item Clearly, $(1,3)$ is the earliest ending meeting, and the algorithm schedules it.
    \item Then the algorithm looks at $(2,4)$ and skips it, as it conflicts with $(1,3)$, which is already scheduled.
    \item It then checks $(4,5)$, and since there is no conflict, it is scheduled.
    \item Next, it looks at $(4,6)$, and that conflicts with $(4,5)$ and thus gets skipped. 
    \item Finally, the algorithm checks out $(6,8)$ and schedules it. 
\end{enumerate}
We see indeed the algorithm schedules three meetings, producing an optimal solution.
\end{example}
Let us now try to prove the algorithm. The idea is fairly simple. Suppose \textsf{OPT} schedules more meetings than \textsf{ALG}. Then at the first point where \textsf{OPT} disagrees with \textsf{ALG}, it must have scheduled a meeting that ends later than the one \textsf{ALG} schedules. Whatever \textsf{OPT} did afterwards, the algorithm will do no worse than that.
\begin{theorem}
    The algorithm described in \hyperref[section:algo-int]{Section 4.1} always produces an optimal solution  for the interval scheduling problem.
\end{theorem}
\begin{proof}
    Assume algorithm schedules $k$ meetings $\textsf{ALG} = (i_1, i_2,\ldots,i_k)$. Further, suppose for a contradiction that \textsf{OPT} is a better solution producing $t>k$ meetings, and they are $\textsf{OPT}=(j_1,j_2,\ldots,j_t)$. Let both solutions solutions be sorted in starting time; that is, $s_{i_1} < s_{i_2} < \ldots < s_{i_k}$ and $s_{j_1} < s_{j_2} < \ldots < s_{j_k}$.

    Let $p$ be the first meeting where $i_p \neq j_p$. By the design of the algorithm, we have that $i_p$ ends before $j_p$, and therefore $i_p$ ends before $j_{p+1}$ starts.  Hence, the new solution
\begin{equation}
    \textsf{OPT}' = (i_1,i_2,\ldots, i_p , j_{p+1}, j_{p+2},\ldots,j_t)
\end{equation}
is also a valid schedule that still schedules $t$ meetings, as many as \textsf{OPT} does. This is similar to the proof of Theorem~\ref{thm:knap}---$\textsf{OPT}'$ is now closer to \textsf{ALG}. Again, one may repeat this argument. Eventually, we get to an $\textsf{OPT}'$ where $i_p =j_p$ for all $p\leq k$, and the schedules would look like following
\begin{align*}
    \textsf{ALG} & = (i_1, i_2,\ldots, i_k)\\
    \textsf{OPT}' &= (i_1,i_2,\ldots,i_k,j_{k+1},\ldots, j_t).
\end{align*}
The first $k$ meetings are exactly the same, but $\textsf{OPT}'$ continues to schedule $j_{k+1},\ldots,j_t$ after $i_k$. This contradicts the design of the algorithm because if $j_{k+1}$ is still available to be scheduled at the end of \textsf{ALG}, the algorithm would schedule it.
\end{proof}

\section{Summary}
\label{section:summary}
We have discussed the greedy paradigm in algorithm design and showed that it gives algorithms that solve fractional knapsack and interval scheduling problems. The proofs for correctness of these algorithms are fairly tricky, and we will see more examples next lecture.
\end{document}
